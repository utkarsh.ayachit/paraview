vtk_module_test_data(
      Data/can.ex2
)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  --client
  TestClientApplication,TestPVApplication.cxx,NO_VALID)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  --server
  TestServerApplication,TestPVApplication.cxx,NO_VALID)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  TestDataArraySelectionWrapping.cxx,NO_VALID
  TestProxyDefinitionsIterator.cxx,NO_VALID
  TestSMInputProperty.cxx,NO_VALID)

# TODO
#vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
#  --batch
#  TestBatchApplication,TestPVApplication.cxx,NO_VALID)
vtk_test_cxx_executable(vtkRemotingServerManagerCxxTests tests)


if (FALSE) # FIXME: ASYNC
if (PARAVIEW_USE_QT)
  find_package(Qt5 REQUIRED COMPONENTS Test)
  set(CMAKE_AUTOMOC 1)
  set(UNITTESTS
    SMProxy
    SMProxyLink
    SMDoubleVectorProperty
    SMIntVectorProperty
    SMStringVectorProperty
    SMUndoStack
    SMPropertyHelper
  )
  foreach(name IN LISTS UNITTESTS)
    list(APPEND test_sources Test${name}.cxx)
    list(APPEND extra_sources vtk${name}Test.cxx ${${name}_MOC_SOURCES})
  endforeach()
endif()

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  NO_DATA NO_VALID
  TestAdjustRange.cxx
  TestMultiplexerSourceProxy.cxx
  TestProxyAnnotation.cxx
  TestRecreateVTKObjects.cxx
  TestRemotingCoreConfiguration.cxx
  TestSelfGeneratingSourceProxy.cxx
  TestSessionProxyManager.cxx
  TestSettings.cxx
  TestValidateProxies.cxx
  TestXMLSaveLoadState.cxx)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  NO_DATA NO_VALID NO_OUTPUT
  ${test_sources})

vtk_test_cxx_executable(vtkRemotingServerManagerCxxTests tests
  ${extra_sources})

if (PARAVIEW_USE_QT)
  target_link_libraries(vtkRemotingServerManagerCxxTests PRIVATE Qt5::Test)
endif ()

set_property(SOURCE TestValidateProxies.cxx APPEND
  PROPERTY
    COMPILE_DEFINITIONS "BUILD_SHARED_LIBS=$<BOOL:${BUILD_SHARED_LIBS}>")
endif() # FIXME
